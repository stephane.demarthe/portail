---
title: Analyse théorique de quatre algorithmes de tri
author: Éric Wegrzynowski
date: mai 2019
geometry: width=18cm, height=25cm
graphics: oui
numbersections: oui
---


**Prérequis :** *Les notes qui suivent supposent donnés quatre algorithmes de tri réalisés en Python : le tri sélection, le tri insertion, le tri rapide et le tri fusion (cf [tris.py](tris.py)).  De plus, une analyse expérimentale du nombre de comparaisons a été effectuée.*

Nous allons déterminer de manière théorique le nombre de comparaisons effectuées pour trier une liste de longueur $n$.

Comme les expérimentations qui précèdent le montrent, ce nombre de comparaisons dépend naturellement de l'algorithme de tri utilisé, de la longueur de la liste à trier mais aussi peut-être du contenu de cette liste.

Si nous notons $c_{\mathrm{tri}}(\ell)$ le nombre de comparaisons dans le tri par la méthode $\mathrm{tri}$ d'une liste $\ell$ de longueur $n$, alors ce nombre est compris entre deux bornes $c_{\mathrm{tri}}^{min}(n)$ et $c_{\mathrm{tri}}^{max}(n)$ qui ne dépendent que de la longueur de la liste :
$$ c_{\mathrm{tri}}^{min}(n) \leq c_{\mathrm{tri}}(\ell) \leq c_{\mathrm{tri}}^{max}(n),$$
$c_{\mathrm{tri}}^{min}(n)$ désignant le nombre minimal de comparaisons parmi toutes les listes de longueur $n$, et $c_{\mathrm{tri}}^{max}(n)$ le nombre maximal.

Les analyses théoriques se font uniquement à partir de la lecture de l'algorithme et ne nécessitent pas d'exécution.

# Tri sélection

Soit $\ell$ une liste de longueur $n$.

L'algo du tri sélection s'exprimant avec une boucle pour, et les seules comparaisons d'éléments de $\ell$ se faisant dans la sélection de l'indice du minimum, le nombre de comparaisons du tri sélection de $\ell$ peut s'exprimer par

$$c_{\mathrm{tri\_select}}(\ell) = \sum_{i=0}^{n-2} c_{\mathrm{select\_min}}(\ell, i, n).$$

Il reste à déterminer le nombre de comparaisons $c_{\mathrm{select\_min}}(\ell, i, n)$ pour trouver l'indice du minimum dans la tranche $\ell[i:n]$.

L'algorithme de sélection du minimum s'exprime comme une boucle pour dans laquelle à **chaque étape de l'itération** une seule comparaison est effectuée. Donc

$$ c_{\mathrm{select\_min}}(\ell, i, n) = \sum_{j=i+1}^{n} 1 = n - i.$$

On a donc
$$c_{\mathrm{tri\_select}}(\ell) = \sum_{i=0}^{n-2} (n - i) = \frac{n(n-1)}{2}.$$

On peut noter que ce nombre ne dépend que de la longueur de la liste et donc

$$c_{\mathrm{tri\_select}}(\ell) = c_{\mathrm{tri\_select}}^{min}(n) = c_{\mathrm{tri\_select}}^{max}(n).$$

**Conclusion :**

Le tri sélection effectue $c_{\mathrm{tri\_select}}(n) = \frac{n(n-1)}{2}$ comparaisons d'éléments pour trier une liste de longueur $n$, et ceci quelle que soit la liste.


```python
pylab.plot(list(range(TAILLE_MAX)), [ n*(n-1)/2 for n in range(TAILLE_MAX)], 
           'b', label='Calculé')
pylab.plot(list(range(TAILLE_MAX)), nb_comps_select, 
           'r+', label='Cas moyen (estimé)')
pylab.title('Nbre de comparaisons pour le tri sélection')
pylab.xlabel('taille des listes')
pylab.ylabel('nbre comparaisons')
pylab.legend()
pylab.grid()
```


![png](output_89_0.png)


# Tri insertion

Soit $\ell$ une liste de longueur $n$.

De la même façon que pour le tri sélection, puisque l'algo du tri insertion s'exprime par une boucle pour, et que les seules comparaisons d'éléments de $\ell$ se font dans l'insertion d'un élement dans le début de liste,  le nombre de comparaisons du tri insertion de $\ell$ peut s'exprimer par

$$c_{\mathrm{tri\_insert}}(\ell) = \sum_{i=1}^{n-1} c_{\mathrm{inserer}}(\ell, i).$$

Il reste à déterminer le nombre de comparaisons $c_{\mathrm{inserer}}(\ell, i)$ pour insérer l'élément $\ell[i]$ dans  la tranche $\ell[0:i+1]$.

L'algorithme d'insertion s'exprime par une boucle tant que et à chaque étape de l'itération il n'y a qu'une seule comparaison entre éléments de $\ell$. Le nombre de comparaisons dépend donc du nombre d'étapes, et cela dépend donc du contenu de la liste.

Dans le *meilleur des cas* l'élément d'indice $i$ est plus grand (ou égal) que celui d'indice $i-1$, et dans ce cas une seule comparaison suffit à ranger l'élément $\ell[i]$.

Dans le *pire des cas* cet élément est plus petit que tous ceux qui précèdent et doit donc être comparé (une fois) à chacun d'eux, et dans ce cas $i$ comparaisons sont effectuées.

Ainsi, pour tout $i$ compris entre 1 et $n-1$,  on a
$$ 1 \leq c_{\mathrm{inserer}}(\ell, i) \leq i.$$

On peut déduire de l'analyse de l'insertion que 


$$ \sum_{i=1}^{n-1}1 \leq c_{\mathrm{tri\_insert}}(\ell)\leq \sum_{i=1}^{n-1}i,$$
ce qui donne
$$ n-1 \leq c_{\mathrm{tri\_insert}}(\ell)\leq \frac{n(n-1)}{2}.$$

Existe-t-il des listes pour lesquelles le nombre de comparaisons soit égal à la minoration trouvée ci-dessus ? Ce sont celles pour lesquelles pour tout $i$ compris entre 1 et $n-1$ on $\ell[i-1]\leq \ell[i]$, autrement dit les listes triées. De telles listes réalisent le *meilleur des cas* du tri insertion, et le nombre de comparaisons est égal à 
$$c_{\mathrm{tri\_insert}}^{min}(n) = n-1.$$

Et pour la majoration ? Ce sont les listes triées dans l'ordre inverse qui réalisent le *pire des cas*, pour lequel le nombre de comparaisons est égal à 
$$c_{\mathrm{tri\_insert}}^{max}(n) = \frac{n(n-1)}{2}.$$



```python
pylab.plot(list(range(TAILLE_MAX)), [ n*(n-1)/2 for n in range(TAILLE_MAX)], 
           'b', label='Pire des cas (calculé)')
pylab.plot(list(range(TAILLE_MAX)), [n-1 for n in range(TAILLE_MAX)], 
           'g', label='Meilleur des cas (calculé)')
pylab.plot(list(range(TAILLE_MAX)), nb_comps_insert, 
           'r+', label='Cas moyen (estimé)')
pylab.title('Nbre de comparaisons pour le tri insertion')
pylab.xlabel('taille des listes')
pylab.ylabel('nbre comparaisons')
pylab.legend()
pylab.grid()
```


![png](output_94_0.png)


# Tri rapide

## Cas général

Considérons une liste $\ell$ de longueur $n$.

Si $n=0$ ou $1$, alors nous sommes dans un cas de base du tri rapide, et aucune comparaisons d'éléments n'est effectuée, donc

$$ c_{\mathrm{rapide}}(\ell) = 0.$$

Si $n\geq 2$. Alors le partitionnement amène à trier deux listes : 
* la liste $\ell_1$ des éléments plus petits que le pivot
* et la liste $\ell_2$ des éléments plus grands

dont la somme des longueurs est $n-1$. L'opération de partitionnement est réalisée en comparant le pivot choisi avec chacun des autres éléments de la liste. Le nombre de comparaisons dans le tri rapide de $\ell$ comprend donc

1. le nombre de comparaisons dans l'opération de partitionnement, donc $n-1$ ;
2. le nombre de comparaisons dans le tri rapide de $\ell_1$, donc $c_{\mathrm{rapide}}(\ell_1)$ ;
3. et le nombre de comparaisons dans le tri rapide de $\ell_2$, donc $c_{\mathrm{rapide}}(\ell_2)$.

On a donc lorsque $n\geq 2$

$$ c_{\mathrm{rapide}}(\ell) = c_{\mathrm{rapide}}(\ell_1) + c_{\mathrm{rapide}}(\ell_2) + n-1.$$



## Pire des cas

Dans le pire des cas, cas où à chaque appel récursif le pivot place un élément d'un côté et tous les autres de l'autre, autrement dit $\ell_1$ est vide et $\ell_2$ est de longueur $n-1$ (ou vice-versa).

$$ c_{rapide}^{max}(n) = \left\{\begin{array}{lll}
  0 &\mbox{si} & n \leq 1\\
    c_{rapide}^{max}(n-1) + n-1 &\mbox{ sinon.}&
\end{array}
\right.                       
$$

On en déduit facilement que pour tout entier $n\in\mathbb{N}$ on a

$$ c_{rapide}^{max}(n) = \frac{n(n-1)}{2}.$$

Dans le pire des cas le tri fusion ne se comporte pas mieux que les tri sélection et insertion.

Le pire des cas dans le cas où, à chaque appel récursif, le choix du pivot est le premier élément, correspond au cas où la liste est déjà triée (ou triée dans l'ordre décroissant). C'est le choix de pivot que fait l'implantation du tri rapide dans le module `tris`.


```python
n = 30
compare_cpt(reset=True)
tri_rapide(cree_liste_croissante(n), comp=compare_cpt)
compare_cpt(nb=True) - n*(n-1)//2
```




    0



## Meilleur des cas

Dans le meilleur des cas, cas où à chaque appel récursif le pivot partage la liste en deux listes d'égales longueurs (à 1 près), on a 

$$
c_{rapide}^{min}(n) = \left\{\begin{array}{lll}
  0 &\mbox{si} & n \leq 1\\
c_{rapide}^{min}(\lfloor\frac{n-1}{2}\rfloor) 
+ c_{rapide}^{min}(\lceil\frac{n-1}{2}\rceil) + n-1 &\mbox{ sinon.}&
\end{array}
\right.                       
$$


```python
def c_rapide_min(n):
    '''
    :param n: (int) taille d'une liste
    :return: (int) nbre minimal de comparaisons dans le tri rapide 
                   d'une liste de longueur n.
    :CU: n >= 0
    '''
    if n <= 1:
        return 0
    else:
        n1 = (n-1) // 2
        n2 = n - 1 - n1
        return c_rapide_min(n1) + c_rapide_min(n2) + n - 1
```


```python
[c_rapide_min(n) for n in range(20)]
```




    [0, 0, 1, 2, 4, 6, 8, 10, 13, 16, 19, 22, 25, 28, 31, 34, 38, 42, 46, 50]



## Comparaison des meilleur et pire cas avec les nombres de comparaisons moyens estimés


```python
pylab.plot(list(range(TAILLE_MAX)), [ n*(n-1)/2 for n in range(TAILLE_MAX)], 
           'b', label='Pire des cas (calculé)')
pylab.plot(list(range(TAILLE_MAX)), [ c_rapide_min(n) for n in range(TAILLE_MAX)], 
           'g', label='Meilleur des cas (calculé)')
pylab.plot(list(range(TAILLE_MAX)), nb_comps_rapide, 
           'r', label='Cas moyen (estimé)')
pylab.title('Nbre de comparaisons pour le tri rapide')
pylab.xlabel('taille des listes')
pylab.ylabel('nbre comparaisons')
pylab.legend()
pylab.grid()
```


![png](output_109_0.png)


On peut constater sur ce graphique qu'en moyenne le tri rapide est (probablement) plus proche du meilleur des cas que du pire. Voyons le même graphique sans le pire des cas.


```python
pylab.plot(list(range(TAILLE_MAX)), [ c_rapide_min(n) for n in range(TAILLE_MAX)], 
           'g', label='Meilleur des cas (calculé)')
pylab.plot(list(range(TAILLE_MAX)), nb_comps_rapide, 
           'r', label='Cas moyen (estimé)')
pylab.title('Nbre de comparaisons pour le tri rapide')
pylab.xlabel('taille des listes')
pylab.ylabel('nbre comparaisons')
pylab.legend()
pylab.grid()
```


![png](output_111_0.png)


## Que dire de l'ordre de grandeur asymptotique du coût dans le meilleur des cas ?

On ne va s'intéresser qu'au cas où la longueur des listes est de la forme 
$$ n = 2^p - 1,$$
$p$ étant un entier naturel quelconque.

Pourquoi s'intéresser à de telles longueurs ? Parce qu'après partionnement dans le meilleur des cas on obtient deux listes de longueurs égales à $2^{p-1}-1$. Et cela simplifie considérablement l'équation de récurrence du meilleur des cas puisqu'on n'a plus besoin des parties entières. Et on a alors

$$
c_{rapide}^{min}(2^p - 1) = \left\{\begin{array}{lll}
  0 &\mbox{si} & p \leq 0\\
2c_{rapide}^{min}(2^{p-1}-1) + 2^p-2 &\mbox{ sinon.}&
\end{array}
\right.                       
$$

Cette équation résolue donne 
$$ c_{rapide}^{min}(2^p - 1) = p.2^{p} - 2.2^{p} + 2,$$

ce qui, exprimé en fonction de $n$, donne

$$c_{rapide}^{min}(n) = (n+1)\log_2{(n+1)} - 2n.$$

Cette formule n'est valide que pour des entiers $n$ de la forme $n=2^p-1$. Mais on peut montrer que pour tout entier $n$ on a
$$ (n+1)\log_2{(n+1)} - 2n \leq c_{rapide}^{min}(n) \leq (n+1)\log_2{(n+1)}.$$

On retient de cela que le coût du tri rapide dans le meilleur des cas est en  $\Theta(n\log{n})$ qui exprime l'ordre de grandeur asymptotique en ne retenant que le terme dominant.


```python
from math import log
pylab.plot(list(range(TAILLE_MAX)), [ c_rapide_min(n) for n in range(TAILLE_MAX)], 
           'g', label='Meilleur des cas (calculé)')
pylab.plot(list(range(TAILLE_MAX)), [(n+1)*log(n+1, 2) -2*n 
                                     for n in range(TAILLE_MAX)], 
           'b', label='Formule meilleur cas')
pylab.title('Nbre de comparaisons pour le tri rapide')
pylab.xlabel('taille des listes')
pylab.ylabel('nbre comparaisons')
pylab.legend()
pylab.grid()
```


![png](output_114_0.png)



```python
pylab.plot(list(range(TAILLE_MAX)), [c_rapide_min(n) - ((n+1)*log(n+1, 2)- 2*n) 
                                     for n in range(TAILLE_MAX)], 
           color='b', label='Formule meilleur cas')
pylab.title('Tri rapide différence')
pylab.xlabel('taille des listes')
pylab.ylabel('nbre comparaisons')
pylab.legend()
pylab.grid()
```


![png](output_115_0.png)


# Tri fusion

## Cas général

Comme pour le tri rapide, le tri fusion s'exprimant de manière récursive, le nombre de comparaisons sera défini par une relation de récurrence.

Soit donc $\ell$ une liste de longueur $n$.

Si $n \leq1$, alors
$$c_{\mathrm{tri\_fusion}}(\ell) = 0.$$

Si $n\geq 2$. Alors la séparation amène à trier deux listes $\ell_1$ et $\ell_2$ de longueurs $\lfloor\frac{n}{2}\rfloor$ et $\lceil\frac{n}{2}\rceil$. Cette séparation ne nécessite aucune comparaison.

Le nombre de comparaisons dans le tri fusion de $\ell$ comprend donc

1. le nombre de comparaisons dans chacun des deux appels récursifs pour trier les listes $\ell_1$ et
   $\ell_2$ ;
2. et le nombre de comparaisons pour fusionner les deux listes triées obtenues après ces appels récursifs.

On a donc lorsque $n\geq 2$
$$ c_{\mathrm{tri\_fusion}}(\ell)  = c_{\mathrm{tri\_fusion}}(\ell_1) + c_{\mathrm{tri\_fusion}}(\ell_2) + c_{\mathrm{fusion}}(\ell_1', \ell_2'),$$
où  $\ell_1'$ et $\ell_2'$ sont les versions triées des deux sous-listes.

Il reste donc à évaluer le nombre de comparaisons nécessaires à fusionner deux listes triées de longueurs $\lfloor\frac{n}{2}\rfloor$ et $\lceil\frac{n}{2}\rceil$.

On peut encadrer ce nombre par
$$ \lfloor\frac{n}{2}\rfloor \leq c_{\mathrm{fusion}}(\ell_1', \ell_2') \leq n - 1.$$

La minoration est atteinte lorsque la plus petite des deux listes a ses éléments tous inférieurs à ceux de l'autre, et la majoration lorsque dans la liste résultante de la fusion il y a alternance entre des éléments provenant de chacune des deux listes.

## Meilleur des cas

$$ c_{\mathrm{tri\_fusion}}^{min}(n) = c_{\mathrm{tri\_fusion}}^{min}(\lfloor\frac{n}{2}\rfloor) + c_{\mathrm{tri\_fusion}}^{min}(\lceil\frac{n}{2}\rceil) + \lfloor\frac{n}{2}\rfloor.$$


```python
def c_fusion_min(n):
    '''
    :param n: (int) taille d'une liste
    :return: (int) nbre minimal de comparaisons dans le tri fusion 
                   d'une liste de longueur n.
    :CU: n >= 0
    '''
    if n <= 1:
        return 0
    else:
        n1 = n // 2
        n2 = n - n1
        return c_fusion_min(n1) + c_fusion_min(n2) + n1
```


```python
[c_fusion_min(n) for n in range(20)]
```




    [0, 0, 1, 2, 4, 5, 7, 9, 12, 13, 15, 17, 20, 22, 25, 28, 32, 33, 35, 37]



## Pire des cas

$$ c_{\mathrm{tri\_fusion}}^{max}(n) = c_{\mathrm{tri\_fusion}}^{max}(\lfloor\frac{n}{2}\rfloor) + c_{\mathrm{tri\_fusion}}^{max}(\lceil\frac{n}{2}\rceil) + n-1.$$


```python
def c_fusion_max(n):
    '''
    :param n: (int) taille d'une liste
    :return: (int) nbre maximal de comparaisons dans le tri fusion 
                   d'une liste de longueur n.
    :CU: n >= 0
    '''
    if n <= 1:
        return 0
    else:
        n1 = n // 2
        n2 = n - n1
        return c_fusion_max(n1) + c_fusion_max(n2) + n - 1
```


```python
[c_fusion_max(n) for n in range(20)]
```




    [0, 0, 1, 3, 5, 8, 11, 14, 17, 21, 25, 29, 33, 37, 41, 45, 49, 54, 59, 64]



## Comparaison des meilleur et pire cas avec les nombres de comparaisons moyens estimés


```python
pylab.plot(list(range(TAILLE_MAX)), [c_fusion_max(n) for n in range(TAILLE_MAX)], 
           'b', label='Pire des cas (calculé)')
pylab.plot(list(range(TAILLE_MAX)), [c_fusion_min(n) for n in range(TAILLE_MAX)], 
           'g', label='Meilleur des cas (calculé)')
pylab.plot(list(range(TAILLE_MAX)), nb_comps_fusion, 
           'r', label='Cas moyen (estimé)')
pylab.title('Nbre de comparaisons pour le tri fusion')
pylab.xlabel('taille des listes')
pylab.ylabel('nbre comparaisons')
pylab.legend()
pylab.grid()
```


![png](output_129_0.png)


## Ordre de grandeur asymptotique

### Meilleur des cas

On considère des listes de longueur $n=2^p$, $p\in\mathbb{N}$.

L'équation de récurrence s'exprime alors, pour $p\leq 1$ sous la forme

$$c_{\mathrm{tri\_fusion}}^{min}(2^p) = 2c_{\mathrm{tri\_fusion}}^{min}(2^{p-1}) + 2^{p-1},$$
et pour $p=0$ on a
$$c_{\mathrm{tri\_fusion}}^{min}(1) = 0.$$

On peut déterminer alors que pour tout entier $p$ on a
$$ c_{\mathrm{tri\_fusion}}^{min}(2^p) = p2^{p-1},$$
ce qui ramené en $n$ donne
$$ c_{\mathrm{tri\_fusion}}^{min}(n) = \frac{n\log_2{n}}{2}.$$


```python
pylab.plot(list(range(TAILLE_MAX)), [c_fusion_min(n) for n in range(TAILLE_MAX)], 
           'g', label='Meilleur des cas (calculé)')
pylab.plot(list(range(1, TAILLE_MAX)), [n*log(n, 2)/2 
                                     for n in range(1, TAILLE_MAX)], 
           'b', label='Formule meilleur cas')
pylab.title('Nbre de comparaisons pour le tri fusion')
pylab.xlabel('taille des listes')
pylab.ylabel('nbre comparaisons')
pylab.legend()
pylab.grid()
```


![png](output_133_0.png)


Ce graphique semble montrer que l'approximation du meilleur des cas par $\frac{n\log_2{n}}{2}$ est plutôy bonne quoique légèrement supérieure à la réalité. 

Voyons ce que donne la différence.


```python
pylab.plot(list(range(1, TAILLE_MAX)), [n*log(n, 2)/2 - c_fusion_min(n) 
                                     for n in range(1, TAILLE_MAX)], 
           color='b', label='différence formule meilleur cas')
pylab.title('Tri fusion différence')
pylab.xlabel('taille des listes')
pylab.ylabel('nbre comparaisons')
pylab.legend()
pylab.grid()
```


![png](output_135_0.png)


### Pire des cas

Toujours pour des listes de longueur $n=2^p$, $p\in\mathbb{N}$.

L'équation de récurrence s'exprime alors, pour $p\leq 1$ sous la forme

$$c_{\mathrm{tri\_fusion}}^{max}(2^p) = 2c_{\mathrm{tri\_fusion}}^{max}(2^{p-1}) + 2^p - 1,$$
et pour $p=0$ on a
$$c_{\mathrm{tri\_fusion}}^{max}(1) = 0.$$

On peut déterminer alors que pour tout entier $p$ on a
$$ c_{\mathrm{tri\_fusion}}^{max}(2^p) = p2^{p} - 2^p + 1,$$
ce qui ramené en $n$ donne
$$ c_{\mathrm{tri\_fusion}}^{max}(n) = n\log_2{n} - n + 1.$$


```python
pylab.plot(list(range(TAILLE_MAX)), [c_fusion_max(n) for n in range(TAILLE_MAX)], 
           'g', label='Pire des cas (calculé)')
pylab.plot(list(range(1, TAILLE_MAX)), [n*log(n, 2) - n  + 1
                                     for n in range(1, TAILLE_MAX)], 
           'b', label='Formule pire cas')
pylab.title('Nbre de comparaisons pour le tri fusion')
pylab.xlabel('taille des listes')
pylab.ylabel('nbre comparaisons')
pylab.legend()
pylab.grid()
```


![png](output_138_0.png)


Bonne approximation.

Voyons la différence.


```python
pylab.plot(list(range(1, TAILLE_MAX)), [c_fusion_max(n) - (n*log(n,2) - n + 1)
                                     for n in range(1, TAILLE_MAX)], 
           color='b', label='différence formule pire cas')
pylab.title('Tri fusion différence')
pylab.xlabel('taille des listes')
pylab.ylabel('nbre comparaisons')
pylab.legend()
pylab.grid()
```


![png](output_140_0.png)


# Conclusion

Les analyses théoriques des tris confirment globalement ce qu'on avait pu établir expérimentalement. Elles nous ont appris néanmoins deux points que l'étude expérimentale ne révélait pas :

1. dans le meilleur des cas le tri insertion est linéaire.

2. le tri fusion est meilleur que le tri rapide dans le pire des cas. Cependant dans la pratique, le tri rapide est plus performant que le tri fusion car il opère sur place.

Voici un tableau récapitulatif 

|tri       | meilleur des cas        | pire des cas      |
|----------|-------------------------|-------------------|
|sélection | $\frac{n(n-1)}{2}$      | $\frac{n(n-1)}{2}$|
|insertion | $n-1$                   | $\frac{n(n-1)}{2}$|
|rapide    | $\Theta(n\log{n})$ | $\frac{n(n-1)}{2}$|
|fusion    | $\Theta(n\log{n})$ | $\Theta(n\log{n})$|



